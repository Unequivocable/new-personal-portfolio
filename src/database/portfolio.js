const data = [
  {
    eventkey: "first",
    tabname: "Portfolio 1",
    panetext: "This is the description for my first portfolio item",
    location: "/scroll",
    className: "portfolio"
  },
  {
    eventkey: "second",
    tabname: "Portfolio 2",
    panetext: "This is the description for my second portfolio item",
    location: "/about",
    className: "portfolio"
  },
];

export default data;
